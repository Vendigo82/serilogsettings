﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;

namespace SPR.Serilog.Settings
{
    public static class Factory
    {
        public class Settings
        {
            public string FileName { get; set; } = "Logs/startup.txt";
        }

        public class SeqSettings
        {
            public bool Enabled { get; set; } = false;

            public string ServerUrl { get; set; } = "http://localhost:5341";

            public string ApiKey { get; set; }
        }

        public static ILogger CreateBootstrapLogger(Action<Settings> adjustSettings = null)
        {
            var settings = new Settings();
            adjustSettings?.Invoke(settings);

            return new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                //.ReadFrom.AppSettings()
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(settings.FileName)
                .CreateBootstrapLogger();
        }

        public static LoggerConfiguration ConfigureSerilog(this LoggerConfiguration config, HostBuilderContext host, IServiceProvider services)
        {
            var seq = host.Configuration.GetSection("Seq").Get<SeqSettings>();

            config.WriteTo.File(
                        "Logs/log.txt",
                        shared: true,
                        rollingInterval: RollingInterval.Month,
                        rollOnFileSizeLimit: true,
                        outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message} ({SourceContext}){NewLine}{Exception}");

            if (seq?.Enabled == true)
                config.WriteTo.Seq(
                        seq.ServerUrl,
                        apiKey: seq.ApiKey,
                        controlLevelSwitch: new LoggingLevelSwitch());

            config.Enrich.FromLogContext();
            config.ReadFrom.Services(services);
            config.MinimumLevel.Information();
            config.MinimumLevel.Override("Microsoft", LogEventLevel.Warning);
            config.MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning);
            config.MinimumLevel.Override("System", LogEventLevel.Warning);
            config.ReadFrom.Configuration(host.Configuration);

            return config;
        }

        public static IHostBuilder UseDefaultSerilog(this IHostBuilder builder) => builder
            .UseSerilog((context, services, config) => config.ConfigureSerilog(context, services));
    }
}
